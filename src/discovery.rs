use async_std::net::{UdpSocket, IpAddr};
use async_std::sync::Arc;
use async_std::task;
use ed25519_dalek::PublicKey;

use std::collections::HashSet;

use crate::keypair::SSBPublicKey;
use crate::peer::{Peer, Address, Protocol, Handshake};

pub struct Server;

impl Server {

    pub async fn recv(sender: async_std::channel::Sender<Peer>, pubkey: Arc<PublicKey>) {
        let socket = UdpSocket::bind(":::8008").await.unwrap();
        let mut buf = [0u8; 1024];
        let mut old = HashSet::new();

        loop {
            let (amt, _) = socket.recv_from(&mut buf).await.unwrap();
            let buf = &mut buf[..amt];
            let packet = String::from_utf8(buf.to_vec()).unwrap();
            log::debug!("Recieved discovery beacon: {}", packet);
            let peer = Codec::decode(packet.as_str());

            if peer.key.to_base64() != pubkey.to_base64() && !old.contains(&peer) {
                old.insert(peer.clone());
                sender.send(peer).await.unwrap();
            }
        }
    }

    pub async fn send(peer: Arc<Peer>) {
        let socket = UdpSocket::bind(":::0").await.unwrap();
        let msg = Codec::encode(Arc::try_unwrap(peer).unwrap());
        let buf = msg.as_bytes();

        socket.set_broadcast(true).unwrap();

        loop {
            log::debug!("Sending discovery beacon");
            socket.send_to(&buf, "255.255.255.255:8008").await.unwrap();
            socket.send_to(&buf, "[FF02::1]:8008").await.unwrap();
            task::sleep(std::time::Duration::from_secs(1)).await;
        }
    }
}

pub struct Codec;

impl Codec {
    // TODO: do this properly
    pub fn encode(peer: Peer) -> String {
        peer.addresses
            .iter()
            .map(|address| {
                let proto = match address.protocol {
                    Protocol::Net => "net",
                    Protocol::Ws => "ws",
                    Protocol::Wss => "wss",
                };
                let hs = match address.handshake {
                    Handshake::Shs => "shs",
                    Handshake::Shs2 => "shs2",
                };
                format!(
                    "{}:{}:{}~{}:{}",
                    proto,
                    address.host,
                    address.port,
                    hs,
                    peer.key.to_base64(),
                )
            })
            .collect::<Vec<String>>()
            .join(";")
    }

    // TODO: do this properly
    pub fn decode(packet: &str) -> Peer {
        let mut key = Option::None;
        let addresses = packet
            .split(';')
            .map(|address| {
                let mut address = address.splitn(2, '~');

                let mut network = address.next().unwrap().splitn(3, ':');
                let protocol = match network.next().unwrap() {
                    "net" => Protocol::Net,
                    "ws" => Protocol::Ws,
                    "wss" => Protocol::Wss,
                    _ => panic!("unknown protocol"),
                };
                let host = IpAddr::V4(network.next().unwrap().parse().unwrap());
                let port = network.next().unwrap().parse().unwrap();

                let mut info = address.next().unwrap().splitn(2, ':');
                let handshake = match info.next().unwrap() {
                    "shs" => Handshake::Shs,
                    "shs2" => Handshake::Shs2,
                    _ => panic!("unknown handshake"),
                };
                let pubkey = SSBPublicKey::from_base64(info.next().unwrap());
                if key == Option::None {
                    key = Some(pubkey);
                } else if key.unwrap() != pubkey {
                    panic!("unexpected pubkey");
                }

                Address {
                    protocol,
                    host,
                    port,
                    handshake,
                }
            })
            .collect();
        Peer {
            addresses,
            key: key.unwrap(),
        }
    }
}
