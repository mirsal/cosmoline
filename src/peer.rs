use async_std::net::IpAddr;
use ed25519_dalek::PublicKey;

use std::hash::{Hash, Hasher};

#[derive(Clone)]
#[derive(Debug)]
pub enum Protocol {
    Net,
    Ws,
    Wss,
}

#[derive(Clone)]
#[derive(Debug)]
pub enum Handshake {
    Shs,
    Shs2,
}

#[derive(Clone)]
#[derive(Debug)]
pub struct Address {
    pub protocol: Protocol,
    pub host: IpAddr,
    pub port: u16,
    pub handshake: Handshake,
}

impl Address {
    pub fn new(protocol: Protocol, host: IpAddr, port: u16, handshake: Handshake) -> Self {
        Self {
            protocol,
            host,
            port,
            handshake,
        }
    }
}

#[derive(Clone)]
#[derive(Debug)]
pub struct Peer {
    pub addresses: Vec<Address>,
    pub key: PublicKey,
}

impl Peer {
    pub fn new(addresses: Vec<Address>, key: PublicKey) -> Self {
        Self { addresses, key }
    }
}

impl Hash for Peer {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.key.to_bytes().hash(state);
    }
}

impl std::cmp::PartialEq for Peer {
    fn eq(&self, other: &Self) -> bool {
        self.key == other.key
    }
}
impl std::cmp::Eq for Peer {}
